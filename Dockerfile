FROM ubuntu
MAINTAINER Radu Graur <radu.graur@gmail.com>

# Install PHP5
RUN apt-get update && apt-get -y install php5 php5-mcrypt php5-mysqlnd php5-redis php5-gd

# Cleanup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /opt/data
VOLUME ["/opt/data"]
WORKDIR /opt/data

ENTRYPOINT ["php", "artisan"]
CMD ["--help"]

