# Artisan Docker image #

### Base image ###
* official [Ubuntu image](https://registry.hub.docker.com/_/ubuntu/)

### Installed packages ###
* php5
* php5-mcrypt
* php5-mysqlnd
* php5-redis
* php5-gd

### Run ###
* build image: `docker build -t artisan .`
* container access: `docker run --privileged=true --volumes-from data-container --rm artisan <command>`

